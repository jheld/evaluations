import os
import pickle

from django.conf import settings
from django.contrib import admin
from django.utils.safestring import mark_safe

from wand.image import Image
from PIL import Image as PImage
import numpy as np

from network2 import load
from run_evaluation import run_it

# Register your models here.
from administration.models import Instructor, Course, Evaluation, EvaluationResult, EvaluationLongFormResult, \
    EvaluationSourceImage, TrainedNetwork, Semester, Department, DepartmentSemesterInstance, MyUser


@admin.register(Semester)
class SemesterAdmin(admin.ModelAdmin):
    pass


@admin.register(Department)
class DepartmentAdmin(admin.ModelAdmin):
    pass


@admin.register(DepartmentSemesterInstance)
class DepartmentSemesterInstanceAdmin(admin.ModelAdmin):
    pass


@admin.register(Instructor)
class InstructorAdmin(admin.ModelAdmin):
    pass


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    pass


@admin.register(TrainedNetwork)
class TrainedNetworkAdmin(admin.ModelAdmin):
    pass


@admin.register(Evaluation)
class EvaluationAdmin(admin.ModelAdmin):
    list_filter = ['course__identifier', 'course__semester', 'status']

    def create_source_images(self, request, queryset):
        for item in queryset:
            item_as_pdf = Image(filename=item.source_file.path, resolution=300)
            item_as_pdf.resize(585, 764)
            item_as_pdf.save(filename=item.source_file.path+'.jpg')
            for i in range(0, len(item_as_pdf.sequence)):
                converted_file_name = u'{}-{}{}'.format(item.source_file.path, i, '.jpg')
                EvaluationSourceImage.objects.update_or_create(page=i+1, evaluation=item,
                                                               defaults=dict(image_file=converted_file_name, ))
    create_source_images.short_description = 'Create Images'

    actions = ['create_source_images', ]


@admin.register(EvaluationResult)
class EvaluationResultAdmin(admin.ModelAdmin):
    list_display = [u'evaluation', u'question_number', u'answer_number', u'hits']
    list_filter = ['question_number', 'answer_number', ]


@admin.register(MyUser)
class MyUserAdmin(admin.ModelAdmin):
    pass


@admin.register(EvaluationSourceImage)
class EvaluationSourceImageAdmin(admin.ModelAdmin):
    def evaluate(self, request, queryset):
        # network_name = '/home/jason/PycharmProjects/DeepLearningPython35/eval_network_epoch_40_hidden_40_eta_0.015_lmbda_0.0_n_5'
        for item in queryset.filter(page=1):
            course = item.evaluation.course
            network_name = course.trained_network.network_file.path if course.trained_network else None
            if not network_name:
                dsi = DepartmentSemesterInstance.objects.filter(department=course.department,
                                                                semester=course.semester,
                                                                trained_network__isnull=False).last()
                if dsi:
                    network_name = dsi.trained_network.network_file.path \
                        if dsi and dsi.trained_network and dsi.trained_network.network_file \
                        else None
            if network_name:
                eval_marked_file_name, parsed_rows, file_name = run_it(network_name,
                                                                       item.image_file.path,
                                                                       item.image_file.path, '', True, True, x_size=23, y_size=23, parse_threshold=0.9975)
                print(len(parsed_rows))
                print(file_name)
                item.parsed_evaluation = pickle.dumps(parsed_rows)
                item.form_marked = eval_marked_file_name
                item.save()
    evaluate.short_description = 'Run the form on the network'
    actions = ['evaluate', u'make_results', ]

    def make_results(self, request, queryset):
        from wand.display import display
        for item in queryset.filter(page=1):
            parsed_rows = item.parsed_evaluation
            parsed_rows = pickle.loads(parsed_rows)
            if item.evaluation.evaluated_forms_file.path.endswith(u'.pdf'):
                ev_form_im = Image(filename=item.evaluation.evaluated_forms_file.path)
                for ev_idx, ev_page in enumerate(ev_form_im.sequence):
                    ev_page.resolution = (300, 300)
                    ev_page.resize(585, 768)
                    Image(ev_page).save(filename=item.evaluation.evaluated_forms_file.path +  '-' + str(ev_idx) + '.jpg')
            item_idx = 0
            while os.path.exists(item.evaluation.evaluated_forms_file.path + '-'+str(item_idx)+'.jpg'):
                item_file_path = item.evaluation.evaluated_forms_file.path + '-'+str(item_idx)+'.jpg'
                im = PImage.open(item_file_path)
                im = im.convert(u'L')
                for r_idx, row in enumerate(parsed_rows):
                    crop_values = []
                    for idx, slot in enumerate(row):
                        coordinates = slot[1].cr_res.box
                        box = (coordinates[0], coordinates[2], coordinates[1], coordinates[3], )
                        cropped = im.crop(box)
                        cropped_array = list(np.array(cropped).reshape(cropped.size[0] * cropped.size[1]))
                        crop_values.append([idx, cropped_array])
                    likely_filled = min([sum(cv[1]) for cv in crop_values])
                    likely_blank = max([sum(cv[1]) for cv in crop_values])
                    # if the darkest is within some shade threshold to the blank one, then it's likely not filled
                    if likely_blank - 255*4 <= likely_filled:
                        continue
                    best_slot = sorted(crop_values, key=lambda x: sum(x[1]))[0]
                    er = EvaluationResult.objects.get_or_create(question_number=r_idx + 1,
                                                                evaluation=item.evaluation,
                                                                answer_number=best_slot[0] + 1)[0]
                    er.hits += 1
                    er.save()
                item_idx += 1

    make_results.short_description = 'Create results'

    def admin_image(self, obj):
        """
        May not keep this around. Useful for now.
        :param obj:
        :return:
        """
        if obj.form_marked:
            return mark_safe('<img src="{media}source_files/{form_marked}"/>'.format(media=settings.MEDIA_URL, form_marked=os.path.basename(obj.form_marked.path)))
        else:
            return u''
    admin_image.allow_tags = True
    admin_image.short_description = 'Marked form'

    list_display = [u'evaluation', u'form_marked', u'admin_image', ]


@admin.register(EvaluationLongFormResult)
class EvaluationLongFormResultAdmin(admin.ModelAdmin):
    pass

