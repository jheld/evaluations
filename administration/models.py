from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

# Create your models here.

class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Create and save a user with the given username, email, and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class MyUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(_('email address'), primary_key=True)

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'

    instructor = models.OneToOneField('Instructor', blank=True, null=True, on_delete=models.CASCADE)

    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name


class Instructor(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=30)
    status = models.CharField(max_length=2)


class TrainedNetwork(models.Model):
    network_file = models.FileField()


class Semester(models.Model):
    name = models.CharField(max_length=10)
    year = models.IntegerField()

    def __str__(self):
        return u'{}, {}'.format(self.name, self.year)


class Department(models.Model):
    name = models.CharField(max_length=40)

    def __str__(self):
        return self.name


class DepartmentSemesterInstance(models.Model):
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    trained_network = models.ForeignKey(TrainedNetwork, null=True, on_delete=models.CASCADE)
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE)


class Course(models.Model):
    section = models.IntegerField()
    title = models.CharField(max_length=100)
    identifier = models.CharField(max_length=10)
    instructor = models.ForeignKey(Instructor, on_delete=models.CASCADE)
    semester = models.ForeignKey(Semester, on_delete=models.CASCADE)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    trained_network = models.ForeignKey(TrainedNetwork, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return '{}, {}'.format(self.semester, self.title)


class Evaluation(models.Model):
    source_file = models.FileField(upload_to='source_files/')
    evaluated_forms_file = models.FileField(upload_to='evaluated_forms/')
    course = models.ForeignKey(Course, on_delete=models.CASCADE)
    status = models.CharField(max_length=2)

    def __str__(self):
        return '{course}'.format(course=self.course)


class EvaluationSourceImage(models.Model):
    page = models.IntegerField()
    image_file = models.FileField(upload_to='image_files/')
    evaluation = models.ForeignKey(Evaluation, on_delete=models.CASCADE)
    parsed_evaluation = models.BinaryField(null=True, blank=True)
    form_marked = models.FileField(null=True, blank=True)


class EvaluationResult(models.Model):
    evaluation = models.ForeignKey(Evaluation, on_delete=models.CASCADE)
    question_number = models.IntegerField()
    answer_number = models.IntegerField()
    hits = models.IntegerField(default=0)

    def __str__(self):
        return '{}, {}, {}'.format(self.evaluation, self.question_number, self.answer_number)


class EvaluationLongFormResult(models.Model):
    evaluation = models.ForeignKey(Evaluation, on_delete=models.CASCADE)
    form_file = models.FileField(upload_to='long_form_files/')

