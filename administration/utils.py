import os
import pickle
from collections import defaultdict

import math
import numpy as np
from PIL import Image as PImage


def responses_over_time(start_year, end_year, course_identifier):
    from administration.models import Evaluation, EvaluationResult, Course, Semester, Department, \
        DepartmentSemesterInstance

    hit_info_per_semester = {}
    semesters = Semester.objects.filter(year__range=[start_year, end_year])
    for semester in semesters:
        course = Course.objects.get(identifier=course_identifier, semester=semester)
        evaluations = Evaluation.objects.get(course=course)
        er_objects = EvaluationResult.objects.filter(evaluation=evaluations)
        hits_per_question_slot = defaultdict(int)
        for er in er_objects:
            hits_per_question_slot[(er.question_number, er.answer_number)] = er.hits
        hit_info_per_semester[(semester.name, semester.year)] = hits_per_question_slot
    return hit_info_per_semester


def get_hits(directory='', parsed_row_path='', file_path_rules=None):
    hits_by_question_answer = {}
    parsed_rows = pickle.load(open(parsed_row_path or '/home/jason/box/From_BrotherDevice/20160102123552_001.jpg_eval_network_epoch_30_hidden_30_eta_0.5_lmbda_0.0_n_5_parsed_rows', 'rb'))
    for item in os.listdir(directory or '/home/jason/box/From_BrotherDevice/to_jpg/'):
        correct_file = len(os.path.splitext(item)[0].split('-')) == 2
        if not file_path_rules:
            the_rules = (item.endswith(u'.jpg') and 'resized' not in item and item.startswith(u'20160101213815_001') and 'marked' not in item and correct_file)# and '20160101213815_001-19.jpg' == item:4
        else:
            the_rules = True
        if the_rules:
            # print(item)
            try:
                im = PImage.open('/home/jason/box/From_BrotherDevice/to_jpg/'+item)
            except Exception:
                continue
            im = im.convert(u'L')
            for r_idx, row in enumerate(parsed_rows):
                crop_values = []
                for idx, slot in enumerate(row):
                    coordinates = slot[1].cr_res.box
                    box = (coordinates[0], coordinates[2], coordinates[1], coordinates[3],)
                    cropped = im.crop(box)
                    # if r_idx == 0:
                    #     cropped.show()
                    cropped_array = list(np.array(cropped).reshape(cropped.size[0] * cropped.size[1]))
                    crop_values.append([idx, cropped_array])
                likely_filled = min([sum(cv[1]) for cv in crop_values])
                likely_blank = max([sum(cv[1]) for cv in crop_values])
                # if the darkest is within some shade threshold to the blank one, then it's likely not filled
                if likely_blank - 255 * 4 <= likely_filled:
                    continue
                best_slot = sorted(crop_values, key=lambda x: sum(x[1]))[0]
                hits_by_question_answer[(r_idx + 1, best_slot[0] + 1)] = 1 + hits_by_question_answer.get((r_idx + 1, best_slot[0] + 1), 0)
    return hits_by_question_answer


def make_plots(hits_by_question_answer):
    hits_by_question = defaultdict(int)
    for k, v in hits_by_question_answer.items():
        hits_by_question[k[0]] += v
    for k, v in hits_by_question.items():
        if v != 50:
            print(k)
    print(all(value == 50 for value in hits_by_question.values()))
    hits = [[k, v] for k, v in hits_by_question_answer.items()]
    # print(hits)
    print('\n'.join(list(str(item) for item in sorted(hits, key=lambda x: x[0]))))
    # for key, value in hits_by_question_answer.items():
    #     if key[0] == 1:
    #         print(key, value)
    import matplotlib
    import matplotlib.pyplot as plt
    import matplotlib.patches as mpatches
    import csv
    human_file = open('/home/jason/box/from copy/Documents/School/Umass/thesis/thesis_human_filled_stats.csv', 'r')
    reader = csv.DictReader(human_file)
    rows = list(reader)
    for index in range(16):
        human_ones = [0, 0, 0, 0, 0]
        print(index)
        human_ones[0] = int(rows[index * 5 + 0][u'hits'] or 0)
        human_ones[1] = int(rows[index * 5 + 1][u'hits'] or 0)
        human_ones[2] = int(rows[index * 5 + 2][u'hits'] or 0)
        human_ones[3] = int(rows[index * 5 + 3][u'hits'] or 0)
        human_ones[4] = int(rows[index * 5 + 4][u'hits'] or 0)
        # %matplotlib(inline)
        x = np.arange(10)
        ones_hits = [0, 0, 0, 0, 0]
        for key, value in hits_by_question_answer.items():
            if key[0] == index + 1:
                ones_hits[key[1] - 1] = value
        plt.bar(x, height=[j for i in zip(ones_hits, human_ones) for j in i], color=['g', 'b'])
        plt.xticks(x, ['1', '1', '2', '2', '3', '3', '4', '4', '5', '5'])
        plt.xlabel('Answer Choices')
        plt.ylabel('Number of responses')
        plt.title('Question: {}'.format(index + 1))
        yint = range(int(math.floor(min([j for i in zip(ones_hits, human_ones) for j in i]))),
                     int(math.ceil(max([j for i in zip(ones_hits, human_ones) for j in i])) + 1))
        plt.yticks(yint)
        # plt.show()
        plt.grid()
        green_patch = mpatches.Patch(color='green', label='The machine totals')
        blue_patch = mpatches.Patch(color='blue', label='The human totals')
        plt.legend(handles=[green_patch, blue_patch])
        plt.savefig('plot_{}.png'.format(index + 1))
        plt.clf()


if __name__ == '__main__':
    make_plots(get_hits())

