from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic import TemplateView
from rest_framework.views import APIView

# Create your views here.
from administration.models import EvaluationResult


class IndexView(LoginRequiredMixin, TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        if self.request.user and getattr(self.request.user, 'instructor', None):
            context_data['all_objects'] = EvaluationResult.objects.filter(evaluation__course__instructor=self.request.user.instructor).distinct()
        elif self.request.user:
            context_data['all_objects'] = EvaluationResult.objects.all()

        return context_data
